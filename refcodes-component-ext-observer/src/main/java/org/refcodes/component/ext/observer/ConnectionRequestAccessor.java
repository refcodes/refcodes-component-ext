// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component.ext.observer;

import org.refcodes.component.ConnectionRequest;

/**
 * Provides an accessor for a {@link ConnectionRequest} property.
 */
public interface ConnectionRequestAccessor {

	/**
	 * Retrieves the {@link ConnectionRequest} property from the property.
	 * Determines in which {@link ConnectionRequest} is being requested for a
	 * component.
	 * 
	 * @return Returns the {@link ConnectionRequest} property stored by the
	 *         property.
	 */
	ConnectionRequest getConnectionRequest();

	/**
	 * Provides a mutator for a {@link ConnectionRequest} property.
	 */
	public interface ConnectionRequestMutator {

		/**
		 * Sets the {@link ConnectionRequest} property for the property.
		 * 
		 * @param aConnectionRequest The {@link ConnectionRequest} property to
		 *        be stored by the property.
		 */
		void setConnectionRequest( ConnectionRequest aConnectionRequest );
	}

	/**
	 * Provides a {@link ConnectionRequest} property.
	 */
	public interface ConnectionRequestProperty extends ConnectionRequestAccessor, ConnectionRequestMutator {
		/**
		 * This method stores and passes through the given argument, which is
		 * very useful for builder APIs: Sets the given
		 * {@link ConnectionRequest} (setter) as of
		 * {@link #setConnectionRequest(ConnectionRequest)} and returns the very
		 * same value (getter).
		 * 
		 * @param aConnectionRequest The {@link ConnectionRequest} to set (via
		 *        {@link #setConnectionRequest(ConnectionRequest)}).
		 * 
		 * @return Returns the value passed for it to be used in conclusive
		 *         processing steps.
		 */
		default ConnectionRequest letConnectionRequest( ConnectionRequest aConnectionRequest ) {
			setConnectionRequest( aConnectionRequest );
			return aConnectionRequest;
		}
	}
}
