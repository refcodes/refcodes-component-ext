// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component.ext.observer;

import org.refcodes.component.LifecycleRequest;
import org.refcodes.observer.AbstractMetaDataEvent;
import org.refcodes.observer.EventMetaData;

/**
 * Abstract implementation of the base {@link LifecycleRequestEvent}.
 * 
 * @param <SRC> The type of the event's source in question.
 */
public abstract class AbstractLifecycleRequestEvent<SRC> extends AbstractMetaDataEvent<EventMetaData, SRC> implements LifecycleRequestEvent<SRC> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link AbstractLifecycleRequestEvent} event.
	 *
	 * @param aEventMetaData the event Meta-Data
	 * @param aSource The according source (origin).
	 */
	public AbstractLifecycleRequestEvent( EventMetaData aEventMetaData, SRC aSource ) {
		super( aEventMetaData, aSource );
	}

	/**
	 * Instantiates a new {@link AbstractLifecycleRequestEvent} event.
	 *
	 * @param aLifecycleRequest The lifecycle request.
	 * @param aEventMetaData the event Meta-Data
	 * @param aSource The according source (origin).
	 */
	public AbstractLifecycleRequestEvent( LifecycleRequest aLifecycleRequest, EventMetaData aEventMetaData, SRC aSource ) {
		super( aEventMetaData, aSource );
	}

	/**
	 * Instantiates a new {@link AbstractLifecycleRequestEvent} event.
	 *
	 * @param aLifecycleRequest The lifecycle request.
	 * @param aSource The according source (origin).
	 */
	public AbstractLifecycleRequestEvent( LifecycleRequest aLifecycleRequest, SRC aSource ) {
		super( aSource );
	}

	/**
	 * Instantiates a new {@link AbstractLifecycleRequestEvent} event.
	 *
	 * @param aSource The according source (origin).
	 */
	public AbstractLifecycleRequestEvent( SRC aSource ) {
		super( aSource );
	}
}
