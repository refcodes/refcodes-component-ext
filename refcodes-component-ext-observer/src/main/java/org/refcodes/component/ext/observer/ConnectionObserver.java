// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component.ext.observer;

import org.refcodes.exception.VetoException;
import org.refcodes.observer.Observer;

/**
 * An observer for listening to {@link ConnectionEvent} instances.
 * 
 * @param <SRC> The type of the source in question.
 */
public interface ConnectionObserver<SRC> extends Observer<ConnectionEvent<?, SRC>> {

	/**
	 * Signaled in case a device is being opened.
	 * 
	 * @param aEvent The {@link OpenedEvent} signaling a device-open.
	 */
	void onOpendEvent( OpenedEvent<SRC> aEvent );

	/**
	 * Signaled in case a device is being closed.
	 * 
	 * @param aEvent The {@link ClosedEvent} signaling a device-close.
	 */
	void onClosedEvent( ClosedEvent<SRC> aEvent );

	/**
	 * An observer for listening to {@link ConnectionRequestEvent} instances.
	 * 
	 * @param <SRC> The type of the source in question.
	 */
	public interface ConnectionRequestObserver<SRC> extends ConnectionObserver<SRC> {

		/**
		 * In case a device is to be opened, then the {@link OpenEvent} event is
		 * fired.
		 *
		 * @param aEvent The according {@link OpenEvent}.
		 * 
		 * @throws VetoException the veto exception
		 */
		void onOpenEvent( OpenEvent<SRC> aEvent ) throws VetoException;

		/**
		 * In case a device is to be closed, then the {@link CloseEvent} event
		 * is fired.
		 *
		 * @param aEvent The according {@link CloseEvent}.
		 * 
		 * @throws VetoException the veto exception
		 */
		void onCloseEvent( CloseEvent<SRC> aEvent ) throws VetoException;
	}
}
