// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component.ext.observer;

import org.refcodes.component.ConnectionStatus;
import org.refcodes.observer.EventMetaData;

/**
 * Implementation of the {@link ConnectionStatusEvent} for
 * {@link ConnectionStatus#CLOSED}.
 * 
 * @param <SRC> The type of the source in question.
 */
public class ClosedEvent<SRC> extends AbstractConnectionStatusEvent<SRC> implements ClosedAccomplishedEvent<SRC> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link ClosedEvent}.
	 *
	 * @param aEventMetaData The event's Meta-Data
	 * @param aSource The according source (origin).
	 */
	public ClosedEvent( EventMetaData aEventMetaData, SRC aSource ) {
		super( ConnectionStatus.CLOSED, aEventMetaData, aSource );
	}

	/**
	 * Instantiates a new {@link ClosedEvent}.
	 *
	 * @param aSource The according source (origin).
	 */
	public ClosedEvent( SRC aSource ) {
		super( ConnectionStatus.CLOSED, aSource );
	}

	/**
	 * Instantiates a new {@link ClosedEvent}.
	 *
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public ClosedEvent( String aChannel, SRC aSource ) {
		super( ConnectionStatus.CLOSED, new EventMetaData( aChannel ), aSource );
	}

	/**
	 * Instantiates a new {@link ClosedEvent}.
	 * 
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public ClosedEvent( String aChannel, Class<?> aPublisherType, SRC aSource ) {
		super( ConnectionStatus.CLOSED, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Instantiates a new {@link ClosedEvent}.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public ClosedEvent( String aAlias, String aGroup, SRC aSource ) {
		super( ConnectionStatus.CLOSED, new EventMetaData( aAlias, aGroup ), aSource );
	}

	/**
	 * Instantiates a new {@link ClosedEvent}.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public ClosedEvent( String aAlias, String aGroup, String aChannel, SRC aSource ) {
		super( ConnectionStatus.CLOSED, new EventMetaData( aAlias, aGroup, aChannel ), aSource );
	}

	/**
	 * Instantiates a new {@link ClosedEvent}.
	 *
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public ClosedEvent( String aAlias, String aGroup, String aChannel, Class<?> aPublisherType, SRC aSource ) {
		super( ConnectionStatus.CLOSED, new EventMetaData( aAlias, aGroup, aChannel, aPublisherType ), aSource );
	}

	/**
	 * Instantiates a new {@link ClosedEvent}.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aUid The Universal-TID for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public ClosedEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, SRC aSource ) {
		super( ConnectionStatus.CLOSED, new EventMetaData( aAlias, aGroup, aChannel, aUid, aPublisherType ), aSource );
	}
}
