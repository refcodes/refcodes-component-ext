// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component.ext.observer;

import org.refcodes.component.LifecycleStatus;
import org.refcodes.observer.EventMetaData;

/**
 * Implementation of the {@link LifecycleStatusEvent} for
 * {@link LifecycleStatus#DESTROYED}.
 * 
 * @param <SRC> The type of the source in question.
 */
public class DestroyedEvent<SRC> extends AbstractLifecycleStatusEvent<SRC> implements DestroyAccomplishedEvent<SRC> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new {@link DestroyedEvent}.
	 *
	 * @param aEventMetaData The event's Meta-Data
	 * @param aSource The according source (origin).
	 */
	public DestroyedEvent( EventMetaData aEventMetaData, SRC aSource ) {
		super( LifecycleStatus.DESTROYED, aEventMetaData, aSource );
	}

	/**
	 * Instantiates a new {@link DestroyedEvent}.
	 *
	 * @param aSource The according source (origin).
	 */
	public DestroyedEvent( SRC aSource ) {
		super( LifecycleStatus.DESTROYED, aSource );
	}

	/**
	 * Instantiates a new {@link DestroyedEvent}.
	 *
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyedEvent( String aChannel, SRC aSource ) {
		super( LifecycleStatus.DESTROYED, new EventMetaData( aChannel ), aSource );
	}

	/**
	 * Instantiates a new {@link DestroyedEvent}.
	 * 
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyedEvent( String aChannel, Class<?> aPublisherType, SRC aSource ) {
		super( LifecycleStatus.DESTROYED, new EventMetaData( aChannel, aPublisherType ), aSource );
	}

	/**
	 * Instantiates a new {@link DestroyedEvent}.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyedEvent( String aAlias, String aGroup, SRC aSource ) {
		super( LifecycleStatus.DESTROYED, new EventMetaData( aAlias, aGroup ), aSource );
	}

	/**
	 * Instantiates a new {@link DestroyedEvent}.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyedEvent( String aAlias, String aGroup, String aChannel, SRC aSource ) {
		super( LifecycleStatus.DESTROYED, new EventMetaData( aAlias, aGroup, aChannel ), aSource );
	}

	/**
	 * Instantiates a new {@link DestroyedEvent}.
	 *
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyedEvent( String aAlias, String aGroup, String aChannel, Class<?> aPublisherType, SRC aSource ) {
		super( LifecycleStatus.DESTROYED, new EventMetaData( aAlias, aGroup, aChannel, aPublisherType ), aSource );
	}

	/**
	 * Instantiates a new {@link DestroyedEvent}.
	 * 
	 * @param aAlias The alias for the {@link EventMetaData}.
	 * @param aGroup The group for the {@link EventMetaData}.
	 * @param aChannel The channel for the {@link EventMetaData}.
	 * @param aUid The Universal-TID for the {@link EventMetaData}.
	 * @param aPublisherType The publisher type for the {@link EventMetaData}.
	 * @param aSource The according source (origin).
	 */
	public DestroyedEvent( String aAlias, String aGroup, String aChannel, String aUid, Class<?> aPublisherType, SRC aSource ) {
		super( LifecycleStatus.DESTROYED, new EventMetaData( aAlias, aGroup, aChannel, aUid, aPublisherType ), aSource );
	}
}
