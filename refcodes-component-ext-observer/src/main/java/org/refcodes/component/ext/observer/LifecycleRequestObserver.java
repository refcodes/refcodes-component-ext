// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component.ext.observer;

import org.refcodes.observer.EventMetaData;

/**
 * An observer for listening to {@link LifecycleRequestEvent} instances.
 *
 * @param <INIT> the generic type
 * @param <START> the generic type
 * @param <RESUME> the generic type
 * @param <PAUSE> the generic type
 * @param <STOP> the generic type
 * @param <DESTROY> the generic type
 * @param <META> the generic type
 * @param <SRC> The source of the events consumed by the
 *        {@link LifecycleRequestObserver}.
 */
public interface LifecycleRequestObserver<INIT extends InitializeRequestedEvent<SRC>, START extends StartRequestedEvent<SRC>, RESUME extends ResumeRequestedEvent<SRC>, PAUSE extends PauseRequestedEvent<SRC>, STOP extends StopRequestedEvent<SRC>, DESTROY extends DestroyRequestedEvent<SRC>, META extends EventMetaData, SRC> {

	/**
	 * In case a component is to be initialized, then the
	 * {@link InitializeEvent} event is fired.
	 * 
	 * @param aEvent The according {@link InitializeEvent}.
	 */
	void onInitialize( INIT aEvent );

	/**
	 * In case a component is to be initialized, then the {@link StartEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link StartEvent}.
	 */
	void onStart( START aEvent );

	/**
	 * In case a component is to be initialized, then the {@link ResumeEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link ResumeEvent}.
	 */
	void onResume( RESUME aEvent );

	/**
	 * In case a component is to be initialized, then the {@link PauseEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link PauseEvent}.
	 */
	void onPause( PAUSE aEvent );

	/**
	 * In case a component is to be initialized, then the {@link StopEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link StopEvent}.
	 */
	void onStop( STOP aEvent );

	/**
	 * In case a component is to be initialized, then the {@link DestroyEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link DestroyEvent}.
	 */
	void onDestroy( DESTROY aEvent );
}