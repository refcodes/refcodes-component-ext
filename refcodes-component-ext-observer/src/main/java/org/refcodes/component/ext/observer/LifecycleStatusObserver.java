// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component.ext.observer;

import org.refcodes.observer.EventMetaData;

/**
 * The {@link LifecycleStatusObserver} is signaled upon
 * {@link LifecycleStatusEvent} events.
 *
 * @param <INITED> the generic type
 * @param <STARTED> the generic type
 * @param <RESUMED> the generic type
 * @param <PAUSED> the generic type
 * @param <STOPPED> the generic type
 * @param <DESTROYED> the generic type
 * @param <META> the generic type
 * @param <SRC> the generic type
 */
public interface LifecycleStatusObserver<INITED extends InitializeAccomplishedEvent<SRC>, STARTED extends StartAccomplishedEvent<SRC>, RESUMED extends ResumeAccomplishedEvent<SRC>, PAUSED extends PauseAccomplishedEvent<SRC>, STOPPED extends StopAccomplishedEvent<SRC>, DESTROYED extends DestroyAccomplishedEvent<SRC>, META extends EventMetaData, SRC> {

	/**
	 * In case a component is to be initialized, then the
	 * {@link InitializedEvent} event is fired.
	 * 
	 * @param aEvent The according {@link InitializedEvent}.
	 */
	void onInitialized( INITED aEvent );

	/**
	 * In case a component is to be initialized, then the {@link StartedEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link StartedEvent}.
	 */
	void onStarted( STARTED aEvent );

	/**
	 * In case a component is to be initialized, then the {@link ResumedEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link ResumedEvent}.
	 */
	void onResumed( RESUMED aEvent );

	/**
	 * In case a component is to be initialized, then the {@link StartedEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link StartedEvent}.
	 */
	void onPaused( PAUSED aEvent );

	/**
	 * In case a component is to be initialized, then the {@link StoppedEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link StoppedEvent}.
	 */
	void onStopped( STOPPED aEvent );

	/**
	 * In case a component is to be initialized, then the {@link DestroyedEvent}
	 * event is fired.
	 * 
	 * @param aEvent The according {@link DestroyedEvent}.
	 */
	void onDestroyed( DESTROYED aEvent );

}
