// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.component.ext.observer;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.component.InitializeException;
import org.refcodes.component.LifecycleComponent;
import org.refcodes.component.PauseException;
import org.refcodes.component.ResumeException;
import org.refcodes.component.StartException;
import org.refcodes.component.StopException;
import org.refcodes.observer.EventMetaData;
import org.refcodes.runtime.SystemProperty;

public class ObservableLifecycleRequestAutomatonTest implements LifecycleComponent, LifecycleRequestObserver<InitializeRequestedEvent<ObservableLifecycleRequestAutomatonTest>, StartRequestedEvent<ObservableLifecycleRequestAutomatonTest>, ResumeRequestedEvent<ObservableLifecycleRequestAutomatonTest>, PauseRequestedEvent<ObservableLifecycleRequestAutomatonTest>, StopRequestedEvent<ObservableLifecycleRequestAutomatonTest>, DestroyRequestedEvent<ObservableLifecycleRequestAutomatonTest>, EventMetaData, ObservableLifecycleRequestAutomatonTest> {

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	private boolean _hasInitializeEvent = false;
	private boolean _hasStartEvent = false;
	private boolean _hasPauseEvent = false;
	private boolean _hasResumeEvent = false;
	private boolean _hasStopEvent = false;
	private boolean _hasDestroyEvent = false;

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testSunnyDayLifecycle() throws InitializeException, StartException, PauseException, ResumeException, StopException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final ObservableLifecycleRequestMachine<ObservableLifecycleRequestAutomatonTest> theLifecycleAutomaton = new ObservableLifecycleRequestMachine<>( this );
		theLifecycleAutomaton.subscribeObserver( this );
		assertTrue( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertFalse( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// INITIALIZED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.initialize();
		assertTrue( _hasInitializeEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertTrue( theLifecycleAutomaton.isInitialized() );
		assertTrue( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertFalse( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// STARTED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.start();
		assertTrue( _hasStartEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertTrue( theLifecycleAutomaton.isRunning() );
		assertTrue( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertTrue( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// PAUSED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.pause();
		assertTrue( _hasPauseEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertTrue( theLifecycleAutomaton.isPaused() );
		assertTrue( theLifecycleAutomaton.isResumable() );
		assertTrue( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// RESUMED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.resume();
		assertTrue( _hasResumeEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertTrue( theLifecycleAutomaton.isRunning() );
		assertTrue( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertTrue( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// STOPPED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.stop();
		assertTrue( _hasStopEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertTrue( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertFalse( theLifecycleAutomaton.isStoppable() );
		assertTrue( theLifecycleAutomaton.isStopped() );
		assertTrue( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );

		// ---------------------------------------------------------------------
		// DESTROYED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.destroy();
		assertTrue( _hasDestroyEvent );
		assertTrue( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertFalse( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertTrue( theLifecycleAutomaton.isDestroyed() );
	}

	@Test
	public void testRainyDayLifecycle() throws InitializeException, StartException, PauseException, ResumeException, StopException {

		// ---------------------------------------------------------------------
		// ZOMBIE:
		// ---------------------------------------------------------------------

		final ObservableLifecycleRequestMachine<ObservableLifecycleRequestAutomatonTest> theLifecycleAutomaton = new ObservableLifecycleRequestMachine<>( this );
		theLifecycleAutomaton.subscribeObserver( this );
		assertTrue( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertFalse( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}
		try {
			theLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}
		try {
			theLifecycleAutomaton.stop();
			fail( "Component must not be stoppable." );
		}
		catch ( StopException e ) {}

		// ---------------------------------------------------------------------
		// INITIALIZED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.initialize();
		assertTrue( _hasInitializeEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertTrue( theLifecycleAutomaton.isInitialized() );
		assertTrue( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertFalse( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theLifecycleAutomaton.initialize();
			fail( "Component must not be initializeable." );
		}
		catch ( InitializeException e ) {}
		try {
			theLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}
		try {
			theLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}
		try {
			theLifecycleAutomaton.stop();
			fail( "Component must not be stoppable." );
		}
		catch ( StopException e ) {}

		// ---------------------------------------------------------------------
		// STARTED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.start();
		assertTrue( _hasStartEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertTrue( theLifecycleAutomaton.isRunning() );
		assertTrue( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertTrue( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theLifecycleAutomaton.initialize();
			fail( "Component must not be initializable." );
		}
		catch ( InitializeException e ) {}
		try {
			theLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}

		// ---------------------------------------------------------------------
		// PAUSED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.pause();
		assertTrue( _hasPauseEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertTrue( theLifecycleAutomaton.isPaused() );
		assertTrue( theLifecycleAutomaton.isResumable() );
		assertTrue( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theLifecycleAutomaton.initialize();
			fail( "Component must not be initializeable." );
		}
		catch ( InitializeException e ) {}
		try {
			theLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}

		// ---------------------------------------------------------------------
		// RESUMED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.resume();
		assertTrue( _hasResumeEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertTrue( theLifecycleAutomaton.isRunning() );
		assertTrue( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertTrue( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theLifecycleAutomaton.initialize();
			fail( "Component must not be initializeable." );
		}
		catch ( InitializeException e ) {}
		try {
			theLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}

		// ---------------------------------------------------------------------
		// STOPPED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.stop();
		assertTrue( _hasStopEvent );
		assertFalse( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertTrue( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertFalse( theLifecycleAutomaton.isStoppable() );
		assertTrue( theLifecycleAutomaton.isStopped() );
		assertTrue( theLifecycleAutomaton.isDestroyable() );
		assertFalse( theLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theLifecycleAutomaton.initialize();
			fail( "Component must not be initializeable." );
		}
		catch ( InitializeException e ) {}
		try {
			theLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}
		try {
			theLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}
		try {
			theLifecycleAutomaton.stop();
			fail( "Component must not be stoppable." );
		}
		catch ( StopException e ) {}

		// ---------------------------------------------------------------------
		// DESTROYED:
		// ---------------------------------------------------------------------

		theLifecycleAutomaton.destroy();
		assertTrue( _hasDestroyEvent );
		assertTrue( theLifecycleAutomaton.isInitalizable() );
		assertFalse( theLifecycleAutomaton.isInitialized() );
		assertFalse( theLifecycleAutomaton.isStartable() );
		assertFalse( theLifecycleAutomaton.isRunning() );
		assertFalse( theLifecycleAutomaton.isPausable() );
		assertFalse( theLifecycleAutomaton.isPaused() );
		assertFalse( theLifecycleAutomaton.isResumable() );
		assertFalse( theLifecycleAutomaton.isStoppable() );
		assertFalse( theLifecycleAutomaton.isStopped() );
		assertFalse( theLifecycleAutomaton.isDestroyable() );
		assertTrue( theLifecycleAutomaton.isDestroyed() );
		// Annoy the life-cycle:
		try {
			theLifecycleAutomaton.start();
			fail( "Component must not be startable." );
		}
		catch ( StartException e ) {}
		try {
			theLifecycleAutomaton.pause();
			fail( "Component must not be pausable." );
		}
		catch ( PauseException e ) {}
		try {
			theLifecycleAutomaton.resume();
			fail( "Component must not be resumable." );
		}
		catch ( ResumeException e ) {}
		try {
			theLifecycleAutomaton.stop();
			fail( "Component must not be stoppable." );
		}
		catch ( StopException e ) {}
	}

	// /////////////////////////////////////////////////////////////////////////
	// LIFE-CYCLE:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public void stop() throws StopException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "stop" );
		}
	}

	@Override
	public void destroy() {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "destroy" );
		}
	}

	@Override
	public void start() throws StartException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "start" );
		}
	}

	@Override
	public void pause() throws PauseException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "pause" );
		}
	}

	@Override
	public void resume() throws ResumeException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "resume" );
		}
	}

	@Override
	public void initialize() throws InitializeException {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "initialize" );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// OBSERVABLE:
	// /////////////////////////////////////////////////////////////////////////

	@Override
	public void onInitialize( InitializeRequestedEvent<ObservableLifecycleRequestAutomatonTest> aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "onInitialize" );
		}
		_hasInitializeEvent = true;
	}

	@Override
	public void onStart( StartRequestedEvent<ObservableLifecycleRequestAutomatonTest> aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "onStart" );
		}
		_hasStartEvent = true;
	}

	@Override
	public void onResume( ResumeRequestedEvent<ObservableLifecycleRequestAutomatonTest> aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "onResume" );
		}
		_hasResumeEvent = true;
	}

	@Override
	public void onPause( PauseRequestedEvent<ObservableLifecycleRequestAutomatonTest> aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "onPause" );
		}
		_hasPauseEvent = true;
	}

	@Override
	public void onStop( StopRequestedEvent<ObservableLifecycleRequestAutomatonTest> aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "onStop" );
		}
		_hasStopEvent = true;
	}

	@Override
	public void onDestroy( DestroyRequestedEvent<ObservableLifecycleRequestAutomatonTest> aEvent ) {
		if ( SystemProperty.LOG_TESTS.isEnabled() ) {
			System.out.println( "onDestroy" );
		}
		_hasDestroyEvent = true;
	}
}
